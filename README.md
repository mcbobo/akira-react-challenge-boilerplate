Akira React Challenge Boilerplate
=================================

This starter code is a skeleton for building a React application using [Redux](http://redux.js.org/)
and [ECMAScript 6](https://github.com/lukehoban/es6features).

## Quick Start

1. Make sure you have Node 4.x installed (you should have `node` and `npm` commands available)
2. Install all dependencies using `npm install`
3. Start the dev server using `npm run server`
  - If for some reason this doesn't work, try running it manually using
    `NODE_ENV=development node dev_server.js`
4. The server should now be running. Point your browser to http://localhost:3000

If everything worked, you should see a login screen like this:

![Login Screenshot](https://bitbucket.org/repo/kXj5kb/images/1647525941-Screen%20Shot%202016-01-10%20at%207.02.57%20PM.png)

If you run into issues, contact matt@akira.md.
This starter code is loosely based on https://github.com/erikras/react-redux-universal-hot-example,
so you may be able to find solutions there as well. Note that the Akira code has been updated to
use Babel 6 and has been stripped down specifically for use in development.

## Adding Your Code

Your app should be built by modifying and adding to the code in the `src` directory. The directory
structure follows the standard Redux app layout, with some added structure.

A basic login view has been defined, and there are some basic actions for dealing with
authentication. It is up to you to figure out how to implement the async actions for interacting
with the Akira REST API. You will also have to define some new components for your appointment
views.

There are three pre-defined reducers: `auth` for dealing with actions related to authentication,
`app` for actions that transform the app's local state, and `remote` for actions that interact with
the remote REST API. This is only a suggested structure — feel free to organize your code however
you like.

## Hot Reloading

The build environment is configured for hot reloading. This means that when you make changes to your
source code, your browser will be automatically updated to reflect those changes (the dev server
automatically detects code changes and pushes them to the browser). There are some situations where
hot reloading doesn't work, in which case you will have to manually reload your browser.

## Redux Dev Tools

The build environment also has [Redux DevTools](https://github.com/gaearon/redux-devtools) enabled.
When you point your browser to http://localhost:3000, you'll notice a black bar on the right side.
This is a log of all the Redux actions generated in your app, along with the resulting state (i.e.
the output of your reducers).

You can hide the DevTools bar at any time using `Ctrl + H`.

## Working with ES6

This code is written in ES6, but many of the older React code samples you'll find on the web use
traditional JavaScript syntax. See
http://www.newmediacampaigns.com/blog/refactoring-react-components-to-es6-classes for some tips on
understanding how older React code translates to ES6.

Also have a look at https://github.com/lukehoban/es6features and http://es6-features.org/

## How does it all work?

Mostly black magic.

Seriously, you don't really want to know. It's an unholy mix of at least a dozen node tools and
libraries, but the gist of it is that it's powered by [Webpack](https://webpack.github.io/), which
uses a bunch of [Babel](https://github.com/babel/babel) transformations to translate ES6 and React's
JSX into traditional JavaScript. All of it gets compiled into a single build.js file, with
sourcemaps to help map it back to your original code for debugging. There's also a plugin for
webpack which watches the filesystem for changes and pushes transformed code diffs to the browser
for hot reloading.