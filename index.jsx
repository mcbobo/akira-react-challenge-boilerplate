import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './src/configureStore'
import App from './src/containers/App'

import injectTapEventPlugin from 'react-tap-event-plugin'
// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin()

const store = configureStore()

if (process.env.NODE_ENV === 'development') {
  const debugElement = document.getElementById('react-dev-tools')
  //import DevTools from './src/containers/DevTools';
  const DevTools = require('./src/containers/DevTools').default
  render(
    <DevTools store={store} />,
    debugElement
  )
}


const rootElement = document.getElementById('app')
render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
)
