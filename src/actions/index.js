
/** auth **/
export const LOGIN = 'LOGIN'
export const RECEIVE_AUTH = 'RECEIVE_AUTH'

/** appointments **/
export const FETCH_APPOINTMENTS = 'FETCH_APPOINTMENTS'
export const RECEIVE_APPOINTMENTS = 'RECEIVE_APPOINTMENTS'
export const START_NEW_APPOINTMENT = 'START_NEW_APPOINTMENT'
export const ABANDON_NEW_APPOINTMENT = 'ABANDON_NEW_APPOINTMENT'
export const CREATE_APPOINTMENT = 'CREATE_APPOINTMENT'
export const RECEIVE_CREATED_APPOINTMENT = 'RECEIVE_CREATED_APPOINTMENT'
export const FETCH_AVAILABLE_APPOINTMENT_TIMESLOTS = 'FETCH_AVAILABLE_APPOINTMENT_TIMESLOTS'
export const RECEIVE_AVAILABLE_APPOINTMENT_TIMESLOTS = 'RECEIVE_AVAILABLE_APPOINTMENT_TIMESLOTS'


/**
  * The following are what Redux calls "action creators"
  * (see http://redux.js.org/docs/basics/Actions.html).
  *
  * Normally action creators just return an action object, which at its simplest looks like
  * {type: SOME_ACTION}, but they can also be used to perform async actions such as XHR requests.
  * See for example https://github.com/gaearon/redux-thunk
  */

/** authentication **/

export function login(credentials) {
  // IMPLEMENT ME
  alert("I don't do anything useful right now. Implement me in actions/index.js to hit the AKIRA REST API.")
  return {
    type: LOGIN
  }
}

/** appointments **/

export function startNewAppointment() {
  return {
    type: START_NEW_APPOINTMENT
  }
}

export function abandonNewAppointment() {
  return {
    type: ABANDON_NEW_APPOINTMENT
  }
}

export function fetchAppointments() {
  // IMPLEMENT ME
}

export function createAppointment(appointment) {
  // IMPLEMENT ME
}

export function fetchAvailableAppointmentTimeslots() {
  // IMPLEMENT ME
}
