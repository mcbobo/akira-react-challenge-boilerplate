import React from 'react'
import Paper from 'material-ui/lib/paper'
import TextField from 'material-ui/lib/text-field'
import RaisedButton from 'material-ui/lib/raised-button'

import akiraLogo from '../../media/akira.svg'

import './LoginModal.scss'

export default class LoginModal extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { auth } = this.props

    return (
      <Paper className="LoginModal" zDepth={2}>
        <img src={akiraLogo} alt="Akira" />
        <div>{auth.error && auth.error.message}</div>
        <form action="#" onSubmit={ev => this.handleSubmit(ev)}>
            <TextField
              floatingLabelText="Email"
              onChange={ev => this.handleEmailEntryChange(ev)} />
            <TextField
              style={{marginTop: "-15px"}}
              floatingLabelText="Password"
              type="password"
              onChange={ev => this.handlePasswordEntryChange(ev)} />
            <RaisedButton
              style={{marginTop: "15px", float: 'right'}}
              label="Login"
              primary={true}
              type="submit" />
        </form>
      </Paper>
    )
  }

  handleEmailEntryChange(ev) {
    this.setState({email: ev.target.value})
  }

  handlePasswordEntryChange(ev) {
    this.setState({password: ev.target.value})
  }

  handleSubmit(ev) {
    ev.preventDefault()

    const creds = {
      email: this.state.email,
      password: this.state.password
    }

    this.props.login(creds)
  }
}

LoginModal.propTypes = {
  auth: React.PropTypes.object.isRequired,
  login: React.PropTypes.func.isRequired
}
